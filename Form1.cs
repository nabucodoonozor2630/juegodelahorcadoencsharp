﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

// Creado por Nicolas Gomez Z basado en tutorial todos los derechos reservados ©

namespace JuegoAhorcado
{

    public partial class Form1 : Form
    {

        char[] PalabrasAdivinadas; int Oportunidades;
        char[] PalabraSeleccionada;
        char[] Alfabeto;
        String[] Palabras;


        public Form1()
        {
            InitializeComponent();
         
        }

        public void IniciarJuego() 
        {
            musicBatman();
            // Valores iniciales del juego
            flFichasDeJuego.Controls.Clear();
            flFichasDeJuego.Enabled = true;
            picAhorcado.Image = null;
            lblMensaje.Visible = false;
            Oportunidades = 0; // fallo
            btnIniciarJuego.Image = Properties.Resources.Jugando;
            Palabras = new string[] { "Batman", "Jocker", "Batgirl", "Catman", "Gatubela", "Robin", "Wason", "Gotica"};
            Alfabeto = "ABCDEFGHIJKLMNÑOPQRSTUVWXYZ".ToCharArray();

            // Palabra aleatoria - Adivinar
            Random random = new Random();
            int IndicePalabraSeleccionada = random.Next(0, Palabras.Length);
            PalabraSeleccionada = Palabras[IndicePalabraSeleccionada].ToUpper().ToCharArray();
            PalabrasAdivinadas = PalabraSeleccionada;

            // Ciclo que carga el alfabeto en un flowlayaout --> flFichasDeJuego

            foreach (char LetraAlfabeto in Alfabeto)
            {
                Button btnLetra = new Button();
                btnLetra.Tag = "";
                btnLetra.Text = LetraAlfabeto.ToString();
                btnLetra.Width = 60;
                btnLetra.Height = 40;
                btnLetra.Click += Compara;
                btnLetra.ForeColor = Color.White;
                btnLetra.Font = new Font(btnLetra.Font.Name, 15, FontStyle.Bold);
                btnLetra.BackgroundImageLayout = ImageLayout.Center;
                btnLetra.BackColor = Color.Black;
                btnLetra.Name = LetraAlfabeto.ToString();
                flFichasDeJuego.Controls.Add(btnLetra);


            }



            flPalabra.Controls.Clear();

            // Ciclo que agrega la palabra en un flowlayout (Palabra a adivinar)

            for (int IndiceValorLetra = 0; IndiceValorLetra < PalabraSeleccionada.Length; IndiceValorLetra++)
            {
              
                Button Letra = new Button
                {
                    
                    Tag = PalabraSeleccionada[IndiceValorLetra].ToString(),
                    Width = 46,
                    Height = 80,
                    ForeColor = Color.Black,
                    BackColor = Color.Black,
                    Text = "-"
                };
               
                Letra.Font = new Font(Letra.Font.Name, 32, FontStyle.Bold);
                Letra.BackgroundImageLayout = ImageLayout.Center;
                Letra.BackColor = Color.White;
                Letra.FlatStyle = FlatStyle.Flat;
                Letra.Name = "Adivinado" + IndiceValorLetra.ToString();
                Letra.BackgroundImage = Properties.Resources.Arcetijo2;
                flPalabra.Controls.Add(Letra);

            }



        }

        void Compara(object sender, EventArgs e)
        {
          
            boton();
            bool encontrado = false;
            Button btn = (Button)sender;
            btn.BackColor = Color.Black;
            btn.ForeColor = Color.White;
            btn.Enabled = false;

            // Compara la letra seleccionada con las letras de la palabra
            for (int indiceRevisar = 0; indiceRevisar < PalabrasAdivinadas.Length; indiceRevisar++)
            {

                if (PalabrasAdivinadas[indiceRevisar] == char.Parse(btn.Text))
                {

                    // Si existe la letra actualiza la palabra agregando el valor correspondiente
                    Button tbx = this.Controls.Find("Adivinado" + indiceRevisar, true).FirstOrDefault() as Button;
                    tbx.Text = PalabrasAdivinadas[indiceRevisar].ToString();
                    PalabrasAdivinadas[indiceRevisar] = '-';
                    encontrado = true;

                }


            }

            // Verifica si todas las letras de la palabra estan destapadas
           
            bool Ganaste = true;
           

            for (int indiceAnalizadorGanador = 0; indiceAnalizadorGanador < PalabrasAdivinadas.Length; indiceAnalizadorGanador++)
            {
                // Si el usuario tiene letras pendientes por destapar se cambia el estatus
                if (PalabrasAdivinadas[indiceAnalizadorGanador] != '-')
                {
                   
                    Ganaste = false;
                   
                }

            }

            // Si el estatus de la variable no cambia quiere decir el usuario gano.
            if (Ganaste)  {
                Ganar();
                MessageBox.Show("Ganaste ok, ok"); btnIniciarJuego.Image = Properties.Resources.IniciarJuego;
                flFichasDeJuego.Enabled = false;
            }

            if (!encontrado)
            {
                Oportunidades = Oportunidades + 1;
                picAhorcado.Image = (Bitmap)Properties.Resources.ResourceManager.GetObject("ahorcado" + Oportunidades);


                // Si las oportunidades ya se acabaron(mostrar la palabra)
                if (Oportunidades == 8)
                {

                    lblMensaje.Visible = true;
                    wason();
                    for (int IndiceValorLetra = 0; IndiceValorLetra < PalabraSeleccionada.Length; IndiceValorLetra++)
                    {

                        Button btnLetra = Controls.Find("Adivinado" + IndiceValorLetra, true).FirstOrDefault() as Button;
                        btnLetra.Text = btnLetra.Tag.ToString();

                    }

                    // Desactiva las fichas y cambia el botón de reinicio
                    flFichasDeJuego.Enabled = false;
                    btnIniciarJuego.Image = Properties.Resources.IniciarJuego;

                }

            }


        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void lblMensaje_Click(object sender, EventArgs e)
        {

        }

        private void label1_Click_1(object sender, EventArgs e)
        {

        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            IniciarJuego();
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }

        private void flowLayoutPanel2_Paint(object sender, PaintEventArgs e)
        {

        }

        private void Form1_Load(object sender, EventArgs e)
        {
            IniciarJuego();
        }

        private void IniciarJuego_Click(object sender, EventArgs e)
        {
            // iniciar Juego
         

        }

        private void boton()
        {
            // iniciar Juego
            System.IO.Stream str = Properties.Resources.boton;
            System.Media.SoundPlayer snd = new System.Media.SoundPlayer(str);
            snd.Play();

        }

        private void wason()
        {
            // iniciar Juego
            System.IO.Stream str = Properties.Resources.wason;
            System.Media.SoundPlayer snd = new System.Media.SoundPlayer(str);
            snd.Play();

        }


        public void musicBatman()
        {
            // iniciar Juego
            System.IO.Stream str = Properties.Resources.batman;
            System.Media.SoundPlayer snd = new System.Media.SoundPlayer(str);
            snd.Play();

        }


        public void Ganar()
        {
            // iniciar Juego
            System.IO.Stream str = Properties.Resources.ganar;
            System.Media.SoundPlayer snd = new System.Media.SoundPlayer(str);
            snd.Play();

        }




    }


    // Creado por Nicolas Gomez Z basado en tutorial todos los derechos reservados ©

}